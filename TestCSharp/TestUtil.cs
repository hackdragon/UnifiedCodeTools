using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Uct;

namespace TestCSharp
{
    [TestClass]
    public class TestUtilString
    {
        [TestMethod]
        public void TestString()
        {
            var sLeft = "abcd|*|12345".LeftStr("|*|");
            System.Diagnostics.Trace.WriteLine(sLeft);
            Assert.AreEqual("abcd", sLeft, "������");

            var sRight = "abcd|*|12345".RightStr("|*|");
            System.Diagnostics.Trace.WriteLine(sRight);
            Assert.AreEqual("12345", sRight, "�Ҳ����");
        }

        [TestMethod]
        public void TestInt()
        {
            short LoWord = 1, HiWord = 2;
            var nRet = UtilInt.MakeInt(LoWord, HiWord);
            System.Diagnostics.Trace.WriteLine(nRet.ToString("X8"));
            Assert.AreEqual(0x00020001, nRet, "������");

            Assert.AreEqual(0x00020001, UtilInt.MakeInt(1, 2), "������");

           
        }
    }
}
