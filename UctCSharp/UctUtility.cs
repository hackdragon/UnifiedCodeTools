﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uct
{
    /// <summary>
    /// 扩展字符串
    /// </summary>
    public static class UtilString
    {
        /// <summary>
        /// 中间字符串
        /// </summary>
        /// <param name="sThis"></param>
        /// <param name="sStart">开始的字符串</param>
        /// <param name="sEnd">结束的字符串</param>
        /// <returns>返回字符串</returns>
        public static string MidStr(this string sThis, string sStart, string sEnd)
        {
            if (string.IsNullOrEmpty(sThis) || string.IsNullOrEmpty(sStart) || string.IsNullOrEmpty(sEnd)) return string.Empty;
            int nPosBegin = sThis.IndexOf(sStart);
            if (nPosBegin < 0) return string.Empty;
            int nPosEnd = sThis.IndexOf(sEnd, nPosBegin + sStart.Length);
            if (nPosEnd < 0 || nPosEnd < nPosBegin) return string.Empty;
            return sThis.Substring(nPosBegin + sStart.Length, nPosEnd - nPosBegin - sStart.Length);
        }

        /// <summary>
        /// 左侧字符串
        /// </summary>
        /// <param name="sThis"></param>
        /// <param name="sLeft">分割的文本</param>
        /// <returns></returns>
        public static string LeftStr(this string sThis,string sLeft)
        {
            if (string.IsNullOrEmpty(sThis) || string.IsNullOrEmpty(sLeft)) return string.Empty;
            int nPos= sThis.IndexOf(sLeft);
            if (nPos < 0) return string.Empty;
            return sThis.Substring(0, nPos);
        }

        /// <summary>
        /// 右侧字符串
        /// </summary>
        /// <param name="sThis"></param>
        /// <param name="sRight">分割的文本</param>
        /// <returns></returns>
        public static string RightStr(this string sThis, string sRight)
        {
            if (string.IsNullOrEmpty(sThis) || string.IsNullOrEmpty(sRight)) return string.Empty;
            int nPos = sThis.LastIndexOf(sRight);
            if (nPos < 0) return string.Empty;
            return sThis.Substring(nPos + sRight.Length, sThis.Length - nPos - sRight.Length);
        }
    }

    /// <summary>
    /// 扩展int
    /// </summary>
    public static class UtilInt
    {
        /// <summary>
        /// 2个16位数联合成32位
        /// </summary>
        /// <param name="LoWord">低位16</param>
        /// <param name="HiWord">高位16</param>
        /// <returns></returns>
        public static int MakeInt(short LoWord, short HiWord)
        {
            return ((HiWord << 16) | (LoWord & 0xffff));
        }

        /// <summary>
        /// 2个32位数联合成32位
        /// </summary>
        /// <param name="LoWord">低位32</param>
        /// <param name="HiWord">高位32</param>
        /// <returns></returns>
        public static int MakeInt(int LoWord, int HiWord)
        {
            return MakeInt((short)LoWord, (short)HiWord);
        }
    }

}
