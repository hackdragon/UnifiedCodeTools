﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uct
{

    public partial class NativeConstants
    {
        #region GWL 

        /// GWL_WNDPROC -> (-4)
        public const int GWL_WNDPROC = -4;

        /// GWL_HINSTANCE -> (-6)
        public const int GWL_HINSTANCE = -6;

        /// GWL_HWNDPARENT -> (-8)
        public const int GWL_HWNDPARENT = -8;

        /// GWL_STYLE -> (-16)
        public const int GWL_STYLE = -16;

        /// GWL_EXSTYLE -> (-20)
        public const int GWL_EXSTYLE = -20;

        /// GWL_USERDATA -> (-21)
        public const int GWL_USERDATA = -21;

        /// GWL_ID -> (-12)
        public const int GWL_ID = -12;

        /// GWLP_WNDPROC -> (-4)
        public const int GWLP_WNDPROC = -4;

        /// GWLP_HINSTANCE -> (-6)
        public const int GWLP_HINSTANCE = -6;

        /// GWLP_HWNDPARENT -> (-8)
        public const int GWLP_HWNDPARENT = -8;

        /// GWLP_USERDATA -> (-21)
        public const int GWLP_USERDATA = -21;

        /// GWLP_ID -> (-12)
        public const int GWLP_ID = -12;


        #endregion

        #region SWP


        /// SWP_NOSIZE -> 0x0001
        public const int SWP_NOSIZE = 1;

        /// SWP_NOMOVE -> 0x0002
        public const int SWP_NOMOVE = 2;

        /// SWP_NOZORDER -> 0x0004
        public const int SWP_NOZORDER = 4;

        /// SWP_NOREDRAW -> 0x0008
        public const int SWP_NOREDRAW = 8;

        /// SWP_NOACTIVATE -> 0x0010
        public const int SWP_NOACTIVATE = 16;

        /// SWP_FRAMECHANGED -> 0x0020
        public const int SWP_FRAMECHANGED = 32;

        /// SWP_SHOWWINDOW -> 0x0040
        public const int SWP_SHOWWINDOW = 64;

        /// SWP_HIDEWINDOW -> 0x0080
        public const int SWP_HIDEWINDOW = 128;

        /// SWP_NOCOPYBITS -> 0x0100
        public const int SWP_NOCOPYBITS = 256;

        /// SWP_NOOWNERZORDER -> 0x0200
        public const int SWP_NOOWNERZORDER = 512;

        /// SWP_NOSENDCHANGING -> 0x0400
        public const int SWP_NOSENDCHANGING = 1024;

        /// SWP_DRAWFRAME -> SWP_FRAMECHANGED
        public const int SWP_DRAWFRAME = NativeConstants.SWP_FRAMECHANGED;

        /// SWP_NOREPOSITION -> SWP_NOOWNERZORDER
        public const int SWP_NOREPOSITION = NativeConstants.SWP_NOOWNERZORDER;

        /// SWP_DEFERERASE -> 0x2000
        public const int SWP_DEFERERASE = 8192;

        /// SWP_ASYNCWINDOWPOS -> 0x4000
        public const int SWP_ASYNCWINDOWPOS = 16384;

        public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        public static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        public static readonly IntPtr HWND_TOP = new IntPtr(0);
        public static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        #endregion

        #region WS

        /// WS_OVERLAPPED -> 0x00000000L
        public const int WS_OVERLAPPED = 0;

        /// WS_POPUP -> 0x80000000L
        public const int WS_POPUP = -2147483648;

        /// WS_CHILD -> 0x40000000L
        public const int WS_CHILD = 1073741824;

        /// WS_MINIMIZE -> 0x20000000L
        public const int WS_MINIMIZE = 536870912;

        /// WS_VISIBLE -> 0x10000000L
        public const int WS_VISIBLE = 268435456;

        /// WS_DISABLED -> 0x08000000L
        public const int WS_DISABLED = 134217728;

        /// WS_CLIPSIBLINGS -> 0x04000000L
        public const int WS_CLIPSIBLINGS = 67108864;

        /// WS_CLIPCHILDREN -> 0x02000000L
        public const int WS_CLIPCHILDREN = 33554432;

        /// WS_MAXIMIZE -> 0x01000000L
        public const int WS_MAXIMIZE = 16777216;

        /// WS_CAPTION -> 0x00C00000L
        public const int WS_CAPTION = 12582912;

        /// WS_BORDER -> 0x00800000L
        public const int WS_BORDER = 8388608;

        /// WS_DLGFRAME -> 0x00400000L
        public const int WS_DLGFRAME = 4194304;

        /// WS_VSCROLL -> 0x00200000L
        public const int WS_VSCROLL = 2097152;

        /// WS_HSCROLL -> 0x00100000L
        public const int WS_HSCROLL = 1048576;

        /// WS_SYSMENU -> 0x00080000L
        public const int WS_SYSMENU = 524288;

        /// WS_THICKFRAME -> 0x00040000L
        public const int WS_THICKFRAME = 262144;

        /// WS_GROUP -> 0x00020000L
        public const int WS_GROUP = 131072;

        /// WS_TABSTOP -> 0x00010000L
        public const int WS_TABSTOP = 65536;

        /// WS_MINIMIZEBOX -> 0x00020000L
        public const int WS_MINIMIZEBOX = 131072;

        /// WS_MAXIMIZEBOX -> 0x00010000L
        public const int WS_MAXIMIZEBOX = 65536;

        /// WS_TILED -> WS_OVERLAPPED
        public const int WS_TILED = NativeConstants.WS_OVERLAPPED;

        /// WS_ICONIC -> WS_MINIMIZE
        public const int WS_ICONIC = NativeConstants.WS_MINIMIZE;

        /// WS_SIZEBOX -> WS_THICKFRAME
        public const int WS_SIZEBOX = NativeConstants.WS_THICKFRAME;

        /// WS_TILEDWINDOW -> WS_OVERLAPPEDWINDOW
        public const int WS_TILEDWINDOW = NativeConstants.WS_OVERLAPPEDWINDOW;

        /// WS_OVERLAPPEDWINDOW -> (WS_OVERLAPPED     |                              WS_CAPTION        |                              WS_SYSMENU        |                              WS_THICKFRAME     |                              WS_MINIMIZEBOX    |                              WS_MAXIMIZEBOX)
        public const int WS_OVERLAPPEDWINDOW = (NativeConstants.WS_OVERLAPPED
                    | (NativeConstants.WS_CAPTION
                    | (NativeConstants.WS_SYSMENU
                    | (NativeConstants.WS_THICKFRAME
                    | (NativeConstants.WS_MINIMIZEBOX | NativeConstants.WS_MAXIMIZEBOX)))));

        /// WS_POPUPWINDOW -> (WS_POPUP          |                              WS_BORDER         |                              WS_SYSMENU)
        public const int WS_POPUPWINDOW = (NativeConstants.WS_POPUP
                    | (NativeConstants.WS_BORDER | NativeConstants.WS_SYSMENU));

        /// WS_CHILDWINDOW -> (WS_CHILD)
        public const int WS_CHILDWINDOW = NativeConstants.WS_CHILD;


        #endregion

        #region WS_EX
        /// WS_EX_DLGMODALFRAME -> 0x00000001L
        public const int WS_EX_DLGMODALFRAME = 1;

        /// WS_EX_NOPARENTNOTIFY -> 0x00000004L
        public const int WS_EX_NOPARENTNOTIFY = 4;

        /// WS_EX_TOPMOST -> 0x00000008L
        public const int WS_EX_TOPMOST = 8;

        /// WS_EX_ACCEPTFILES -> 0x00000010L
        public const int WS_EX_ACCEPTFILES = 16;

        /// WS_EX_TRANSPARENT -> 0x00000020L
        public const int WS_EX_TRANSPARENT = 32;

        /// WS_EX_MDICHILD -> 0x00000040L
        public const int WS_EX_MDICHILD = 64;

        /// WS_EX_TOOLWINDOW -> 0x00000080L
        public const int WS_EX_TOOLWINDOW = 128;

        /// WS_EX_WINDOWEDGE -> 0x00000100L
        public const int WS_EX_WINDOWEDGE = 256;

        /// WS_EX_CLIENTEDGE -> 0x00000200L
        public const int WS_EX_CLIENTEDGE = 512;

        /// WS_EX_CONTEXTHELP -> 0x00000400L
        public const int WS_EX_CONTEXTHELP = 1024;

        /// WS_EX_RIGHT -> 0x00001000L
        public const int WS_EX_RIGHT = 4096;

        /// WS_EX_LEFT -> 0x00000000L
        public const int WS_EX_LEFT = 0;

        /// WS_EX_RTLREADING -> 0x00002000L
        public const int WS_EX_RTLREADING = 8192;

        /// WS_EX_LTRREADING -> 0x00000000L
        public const int WS_EX_LTRREADING = 0;

        /// WS_EX_LEFTSCROLLBAR -> 0x00004000L
        public const int WS_EX_LEFTSCROLLBAR = 16384;

        /// WS_EX_RIGHTSCROLLBAR -> 0x00000000L
        public const int WS_EX_RIGHTSCROLLBAR = 0;

        /// WS_EX_CONTROLPARENT -> 0x00010000L
        public const int WS_EX_CONTROLPARENT = 65536;

        /// WS_EX_STATICEDGE -> 0x00020000L
        public const int WS_EX_STATICEDGE = 131072;

        /// WS_EX_APPWINDOW -> 0x00040000L
        public const int WS_EX_APPWINDOW = 262144;

        /// WS_EX_OVERLAPPEDWINDOW -> (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE)
        public const int WS_EX_OVERLAPPEDWINDOW = (NativeConstants.WS_EX_WINDOWEDGE | NativeConstants.WS_EX_CLIENTEDGE);

        /// WS_EX_PALETTEWINDOW -> (WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST)
        public const int WS_EX_PALETTEWINDOW = (NativeConstants.WS_EX_WINDOWEDGE
                    | (NativeConstants.WS_EX_TOOLWINDOW | NativeConstants.WS_EX_TOPMOST));

        /// WS_EX_LAYERED -> 0x00080000
        public const int WS_EX_LAYERED = 524288;

        /// WS_EX_NOINHERITLAYOUT -> 0x00100000L
        public const int WS_EX_NOINHERITLAYOUT = 1048576;

        /// WS_EX_LAYOUTRTL -> 0x00400000L
        public const int WS_EX_LAYOUTRTL = 4194304;

        /// WS_EX_COMPOSITED -> 0x02000000L
        public const int WS_EX_COMPOSITED = 33554432;

        /// WS_EX_NOACTIVATE -> 0x08000000L
        public const int WS_EX_NOACTIVATE = 134217728;


        #endregion

        #region GW
        /// GW_HWNDFIRST -> 0
        public const int GW_HWNDFIRST = 0;

        /// GW_HWNDLAST -> 1
        public const int GW_HWNDLAST = 1;

        /// GW_HWNDNEXT -> 2
        public const int GW_HWNDNEXT = 2;

        /// GW_HWNDPREV -> 3
        public const int GW_HWNDPREV = 3;

        /// GW_OWNER -> 4
        public const int GW_OWNER = 4;

        /// GW_CHILD -> 5
        public const int GW_CHILD = 5;

        /// GW_ENABLEDPOPUP -> 6
        public const int GW_ENABLEDPOPUP = 6;

        /// GW_MAX -> 6
        public const int GW_MAX = 6;

        #endregion

        
    }

    public partial class NativeConstants
    {
        public enum GetAncestorFlags
        {
            /// <summary>
            /// Retrieves the parent window. This does not include the owner, as it does with the GetParent function.
            /// </summary>
            GetParent = 1,
            /// <summary>
            /// Retrieves the root window by walking the chain of parent windows.
            /// </summary>
            GetRoot = 2,
            /// <summary>
            /// Retrieves the owned root window by walking the chain of parent and owner windows returned by GetParent.
            /// </summary>
            GetRootOwner = 3
        }
    }
}
