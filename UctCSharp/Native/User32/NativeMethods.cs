﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uct
{

    public partial class NativeMethods
    {

        /// Return Type: BOOL->int
        ///hWnd: HWND->HWND__*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "IsWindow")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool IsWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd);


        /// Return Type: LONG->int
        ///hWnd: HWND->HWND__*
        ///nIndex: int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int GetWindowLong([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, int nIndex);

        /// Return Type: LONG->int
        ///hWnd: HWND->HWND__*
        ///nIndex: int
        ///dwNewLong: LONG->int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SetWindowLong([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, int nIndex, int dwNewLong);

        /// Return Type: BOOL->int
        ///hWnd: HWND->HWND__*
        ///hWndInsertAfter: HWND->HWND__*
        ///X: int
        ///Y: int
        ///cx: int
        ///cy: int
        ///uFlags: UINT->unsigned int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "SetWindowPos")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool SetWindowPos([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, [System.Runtime.InteropServices.InAttribute()] System.IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);


        /// Return Type: BOOL->int
        ///hwnd: HWND->HWND__*
        ///pwi: PWINDOWINFO->tagWINDOWINFO*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetWindowInfo")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool GetWindowInfo([System.Runtime.InteropServices.InAttribute()] System.IntPtr hwnd, ref tagWINDOWINFO pwi);

        /// Return Type: BOOL->int
        ///hwnd: HWND->HWND__*
        ///pti: PTITLEBARINFO->tagTITLEBARINFO*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetTitleBarInfo")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool GetTitleBarInfo([System.Runtime.InteropServices.InAttribute()] System.IntPtr hwnd, ref tagTITLEBARINFO pti);

        /// Return Type: BOOL->int
        ///hWnd: HWND->HWND__*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "DestroyWindow")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool DestroyWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd);


        /// Return Type: BOOL->int
        ///lpRect: LPRECT->tagRECT*
        ///dwStyle: DWORD->unsigned int
        ///bMenu: BOOL->int
        ///dwExStyle: DWORD->unsigned int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "AdjustWindowRectEx")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool AdjustWindowRectEx(ref tagRECT lpRect, uint dwStyle, [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)] bool bMenu, uint dwExStyle);

        /// Return Type: int
        ///hWnd: HWND->HWND__*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetDlgCtrlID")]
        public static extern int GetDlgCtrlID([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd);

        /// Return Type: HWND->HWND__*
        ///hDlg: HWND->HWND__*
        ///nIDDlgItem: int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetDlgItem")]
        public static extern System.IntPtr GetDlgItem([System.Runtime.InteropServices.InAttribute()] System.IntPtr hDlg, int nIDDlgItem);


        /// Return Type: HWND->HWND__*
        ///hWnd: HWND->HWND__*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetTopWindow")]
        public static extern System.IntPtr GetTopWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd);

        /// Return Type: HWND->HWND__*
        ///hWnd: HWND->HWND__*
        ///uCmd: UINT->unsigned int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetWindow")]
        public static extern System.IntPtr GetWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, uint uCmd);

        ///hWnd: HWND->HWND__*
        ///uCmd: UINT->unsigned int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetWindow")]
        public static extern System.IntPtr GetNextWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, uint uCmd);


        /// Return Type: LRESULT->LONG_PTR->int
        ///hWnd: HWND->HWND__*
        ///Msg: UINT->unsigned int
        ///wParam: WPARAM->UINT_PTR->unsigned int
        ///lParam: LPARAM->LONG_PTR->int
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.SysInt)]
        public static extern int SendMessage([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, uint Msg, [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.SysUInt)] uint wParam, [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.SysInt)] int lParam);


        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, StringBuilder lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.LPWStr)] string lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, ref IntPtr lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, int wParam, IntPtr lParam);


        /// Return Type: HWND->HWND__*
        ///hWnd: HWND->HWND__*
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "GetParent")]
        public static extern System.IntPtr GetParent([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd);


        [System.Runtime.InteropServices.DllImport("user32.dll", ExactSpelling = true)]
        public static extern IntPtr GetAncestor(IntPtr hwnd, NativeConstants.GetAncestorFlags flags);

    }
}
