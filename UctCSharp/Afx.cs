﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uct
{
    public class Afx
    {
		public static bool _AfxModifyStyle(IntPtr hWnd, int nStyleOffset, uint dwRemove, uint dwAdd, uint nFlags)
		{
			var dwStyle = NativeMethods.GetWindowLong(hWnd, nStyleOffset);
			var dwNewStyle = (dwStyle & ~dwRemove) | dwAdd;
			if (dwStyle == dwNewStyle)
				return false;

			NativeMethods.SetWindowLong(hWnd, nStyleOffset, (int)dwNewStyle);
			if (nFlags != 0)
			{
				var uFlag = NativeConstants.SWP_NOSIZE | NativeConstants.SWP_NOMOVE | NativeConstants.SWP_NOZORDER | NativeConstants.SWP_NOACTIVATE | nFlags;
				NativeMethods.SetWindowPos(hWnd, IntPtr.Zero, 0, 0, 0, 0, uFlag);
			}
			return true;
		}

		public static IntPtr AfxGetParentOwner(IntPtr hWnd)
		{
			// otherwise, return parent in the Windows sense
			var b = (NativeMethods.GetWindowLong(hWnd, NativeConstants.GWL_STYLE) & NativeConstants.WS_CHILD);
			return b != 0 ? NativeMethods.GetParent(hWnd) : NativeMethods.GetWindow(hWnd, NativeConstants.GW_OWNER);
		}
	}
}
